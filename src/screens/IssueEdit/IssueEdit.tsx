import React from 'react';
import "./IssueEdit.css"

const IssueEdit:React.FC = ()=>{
    return (
        <div>
            <form className="issue-edit-form" action="">
                <div className="issue-edit-section">
                    <div className="issue-field">
                        <div className="issue-field-name">
                              Component/s:
                        </div>
                        <input type="text"/>
                    </div>
                    <div className="issue-field">
                       <div className="issue-field-name">
                       Labels:
                       </div>
                        <input type="text"/>
                    </div>
                    
                    <div className="issue-field">
                        <div className="issue-field-name">
                              Component/s:
                        </div>
                        <input type="text"/>
                    </div>
                    <div className="issue-field">
                       <div className="issue-field-name">
                       Labels:
                       </div>
                        <input type="text"/>
                    </div>
                    
                </div>
                <div className="issue-edit-section">
                    <div className="issue-field">
                        <div className="issue-field-name">
                              Assignee:
                        </div>
                        <input type="text"/>
                    </div>
                    <div className="issue-field">
                       <div className="issue-field-name">
                            Reporter:
                       </div>
                        <input type="text"/>
                    </div>
                    
                    <div className="issue-field">
                        <div className="issue-field-name">
                              Description:
                        </div>
                        <input type="text"/>
                    </div>
                    <div className="issue-field">
                       <div className="issue-field-name">
                            Attachment:
                       </div>
                        <div className="attachment-dropzone">
                            <img src="./Upload.svg" alt=""/>
                            Drop files to attach or browse
                        </div>
                    </div>
                    
                </div>
                <div className="issue-edit-submit-btn">
                    <button>
                        Submit
                        <i className="arrow right"></i>
                    </button>
                </div>
            </form>
        </div>
    )
}

export default IssueEdit;