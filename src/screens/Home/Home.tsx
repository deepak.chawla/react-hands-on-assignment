import React from 'react';
import Card from '../../components/Card/Card'
import "./Home.css"
import data from '../../mock.json';
import IssueEdit from './../IssueEdit/IssueEdit';

const Home: React.FC = () => {

    const renderIssues = (issueType:string)=>{
        if(issueType==="todos"){
            return data.todos.map((item,i)=>{
                return (
                    <Card key={i} {...item}/>
                )
            })
        }else if(issueType==="inProgress"){
            return data.inProgress.map((item,i)=>{
                return (
                    <Card key={i} {...item}/>
                )
            })
        }else{
            return data.done.map((item,i)=>{
                return (
                    <Card key={i} {...item}/>
                )
            })
        }
    }

    return (
        <div>
            <div className="issues-list">
                <div className="issues-section todos-list">
                    <div className="issues-section-name">
                        Todos {data.todos.length}
                    </div>
                    <div className="issues-section-items">
                       {renderIssues("todos")} 
                    </div>
                </div>
                <div className="issues-section inProgress-list">
                    <div className="issues-section-name">
                    In Progress {data.inProgress.length}
                    </div> 
                    <div className="issues-section-items">
                       {renderIssues("inProgress")} 
                    </div>   
                 </div>
                <div className="issues-section done-list">
                    <div className="issues-section-name">
                    Done {data.done.length}
                    </div>
                    <div className="issues-section-items">
                       {renderIssues("done")} 
                    </div>
                </div>
            </div>
            <IssueEdit/>
        </div>
    )
}

export default Home;