import React from 'react';
import { Props } from './types';
import './Card.css'


const Card: React.FC<Props> = ({ id, issueOn, issueName, issueDescription, assignee, priority }) => {
    return (
        <div className="card">
            <div className="card-header">
                <div className="card-header-left">
                    ID:{id}
                </div>
                <div className="card-header-right text-gray">
                    {issueOn}
                </div>
            </div>
            <div className="card-body">
                <div className="card-body-title">
                    {issueName}
                </div>
                <div className="card-body-description text-gray">
                    {issueDescription}
                </div>
            </div>
            <div className="card-footer">
                <div className="card-footer-left">
                    <span className="text-gray">Assignee</span>
                    <div className="assignee-details">
                        <div className="assignee-photo">
                            <img src="./user_1.svg" alt="" />
                        </div>
                        <div className="assignee">
                            <div className="assignee-name">
                                {assignee.name}
                            </div>
                            <div className="assignee-designation text-gray">
                                {assignee.designation}
                            </div>
                        </div>
                    </div>
                </div>
                <div className="card-status-right">
                    <span className="text-gray">Status</span>
                    <div className={priority === 1 ? "green-back status-btn" : priority === 2 ? "yellow-back status-btn" : priority === 3 ? "orange-back status-btn" : "red-back status-btn"}>
                        {priority === 1 ? "Done" : priority === 2 ? "In Progress" : priority === 3 ? "Low Priority" : "High Priority"}
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Card;