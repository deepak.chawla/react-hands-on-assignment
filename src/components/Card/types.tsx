
export interface Props{
    id: number;
    issueId: string;
    issueOn: string;
    issueName: string;
    issueDescription: string;
    assignee: {
        photo: string;
        name: string;
        designation: string;
    };
    priority: number;
}
